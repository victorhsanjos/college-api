<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    public $timestamps = false;

    protected $fillable = ['salary'];

    protected $hidden = ['user'];

    public function user()
    {
        return $this->morphOne('App\User', 'typeable');
    }

    public static function boot()
    {
        parent::boot();

        static::retrieved(function (Teacher $teacher) {
            foreach ($teacher->user->toArray() as $key => $value) {
                $teacher->setAttribute($key, $value);
            }
        });
    }
}
