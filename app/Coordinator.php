<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coordinator extends Model
{
    public $timestamps = false;

    protected $fillable = ['salary'];

    protected $hidden = ['user'];

    public function user()
    {
        return $this->morphOne('App\User', 'typeable');
    }

    public static function boot()
    {
        parent::boot();

        static::retrieved(function (Coordinator $coordinator) {
            foreach ($coordinator->user->toArray() as $key => $value) {
                $coordinator->setAttribute($key, $value);
            }
        });
    }
}
