<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Student extends Model
{
    use SearchableTrait;

    public $timestamps = false;

    protected $fillable = ['registration'];

    protected $hidden = ['user'];

    public function user()
    {
        return $this->morphOne('App\User', 'typeable');
    }

    public function courses()
    {
        return $this->belongsToMany('App\Course');
    }

    public function subjects()
    {
        return $this->belongsToMany('App\Subject');
    }

    public function saveOriginalOnly()
    {
        $dirty = $this->getDirty();

        foreach ($this->getAttributes() as $key => $value) {
            if(!in_array($key, array_keys($this->getOriginal()))) unset($this->{$key});
        }

        $isSaved = $this->save();

        foreach($dirty as $key => $value) {
            $this->setAttribute($key, $value);
        }

        return $isSaved;
    }

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'students.registration' => 10,
            'users.name' => 10,
            'users.email' => 5,
        ],
        'joins' => [
            'users' => ['students.id', 'users.typeable_id'],
        ],
    ];

    public static function boot()
    {
        parent::boot();

        static::retrieved(function (Student $student) {
            foreach ($student->user->toArray() as $key => $value) {
                $student->setAttribute($key, $value);
            }
        });
    }
}
