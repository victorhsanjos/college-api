<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Course extends Model
{
    use SearchableTrait;

    protected $fillable = ['name', 'periods', 'shift'];

    public function subjects()
    {
        return $this->hasMany('App\Subject');
    }

    public function students()
    {
        return $this->belongsToMany('App\Student');
    }

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'courses.name' => 10,
            'courses.periods' => 8,
            'courses.shift' => 5,
        ]
    ];
}
