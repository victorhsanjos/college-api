<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StudentUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $student = $this->route()->parameter('student');
        $user = $student->user;

        return [
            'name' => "required|string|max:255",
            'email' => "required|string|email|max:255|unique:users,email,{$user->getKey()}",
            'password' => 'string:min:6|comfirmed',
            'registration' => "required|string|unique:students,registration,{$student->getKey()}"
        ];
    }
}
