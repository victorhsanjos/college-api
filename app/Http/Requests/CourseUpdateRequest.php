<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CourseUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $course = $this->route()->parameter('course');

        return [
            'name' => "required|unique:courses,name,{$course->getKey()}|max:255",
            'periods' => 'required|integer|max:255',
            'shift' => 'required|integer|max:255'
        ];
    }
}
