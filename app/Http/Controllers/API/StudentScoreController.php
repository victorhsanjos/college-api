<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\ScoreStoreRequest;
use App\Student;
use App\Subject;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StudentScoreController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.basic.once');
        $this->middleware('permission:add student score')->only('store');
        $this->middleware('permission:delete student score')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Subject $subject, Student $student, ScoreStoreRequest $request)
    {
        $subject->students()->attach($student->id, ['score' => $request->score]);

        return response()->json(['scored' => true], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subject $subject, Student $student)
    {
        $subject->students()->detach($student->id);

        return response()->json(null, 204);
    }
}
