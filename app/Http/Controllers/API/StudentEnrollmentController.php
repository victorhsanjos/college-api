<?php

namespace App\Http\Controllers\API;

use App\Course;
use App\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StudentEnrollmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.basic.once');
        $this->middleware('permission:associate student to course')->only('store');
        $this->middleware('permission:dissociate student from course')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Course $course, Student $student)
    {
        $course->students()->attach($student->id);

        return response()->json(['enrolled' => true], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course, Student $student)
    {
        $course->students()->detach($student->id);

        return response()->json(null, 204);
    }
}
