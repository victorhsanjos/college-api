<?php

namespace App\Http\Controllers\API;

use App\Subject;
use App\Http\Requests\SubjectStoreRequest;
use App\Http\Requests\SubjectUpdateRequest;
use App\Http\Resources\Subject as SubjectResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubjectController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.basic.once');
        $this->middleware('permission:browse subjects')->only('index');
        $this->middleware('permission:add subjects')->only('store');
        $this->middleware('permission:read subjects')->only('show');
        $this->middleware('permission:edit subjects')->only('update');
        $this->middleware('permission:delete subjects')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return SubjectResource::collection(Subject::search($request->q)->paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubjectStoreRequest $request)
    {
        return response()->json(Subject::create($request->all()), 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function show(Subject $subject)
    {
        return new SubjectResource($subject);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function update(SubjectUpdateRequest $request, Subject $subject)
    {
        return response()->json( $subject->update($request->all()), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subject $subject)
    {
        return response()->json($subject->delete(), 204);
    }
}
