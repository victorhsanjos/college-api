<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Subject extends Model
{
    use SearchableTrait;

    protected $fillable = ['name', 'course_id'];

    public function students()
    {
        return $this->belongsToMany('App\Student');
    }

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'subjects.name' => 10,
            'courses.name' => 8,
        ],
        'joins' => [
            'courses' => ['subjects.course_id', 'courses.id'],
        ],
    ];
}
