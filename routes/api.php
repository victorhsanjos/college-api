<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth.basic.once')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResources([
    'courses' => 'API\CourseController',
    'subjects' => 'API\SubjectController',
    'students' => 'API\StudentController',
]);

Route::post('/course/{course}/student/{student}/add', 'API\StudentEnrollmentController@store');
Route::delete('/course/{course}/student/{student}/delete', 'API\StudentEnrollmentController@destroy');

Route::post('/subject/{subject}/student/{student}/score/add', 'API\StudentScoreController@store');
Route::delete('/subject/{subject}/student/{student}/score/delete', 'API\StudentScoreController@destroy');
