<?php

namespace Tests\Feature;

use Illuminate\Http\Response;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ManageCoursesTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    protected $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->artisan('db:seed --class RolesAndPermissionsSeeder');

        $this->user = factory('App\User')->states('teacher')->create()
            ->assignRole('teacher');
    }

    /** @test */
    public function a_user_can_browser_courses()
    {
        factory('App\Course', 10)->create();

        $this->get(route('courses.index'), [
                'Authorization' => 'Basic '. base64_encode("{$this->user->email}:password")
            ])
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure([
            'data' => [
                [
                    'id',
                    'name',
                    'periods',
                    'shift',
                    'updated_at',
                    'created_at',
                ]
            ],
            'links' => [
                'first',
                'last',
                'prev',
                'next',
            ],
            'meta' => [
                'current_page',
                'from',
                'last_page',
                'path',
                'per_page',
                'to',
                'total'
            ]
        ]);
    }

    /** @test */
    public function a_user_can_search_courses()
    {
        factory('App\Course')->create(['name' => 'Development']);
        factory('App\Course')->create(['name' => 'Crime']);
        factory('App\Course')->create(['name' => 'Education']);
        factory('App\Course')->create(['name' => 'Journalism']);
        factory('App\Course')->create(['name' => 'English']);

        $this->get(route('courses.index', 'q=Crime'), [
                'Authorization' => 'Basic '. base64_encode("{$this->user->email}:password")
            ])
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonCount(1, 'data');
    }

    /** @test */
    public function a_user_can_read_course()
    {
        $course = factory('App\Course')->create();

        $this->get(route('courses.show', $course), [
                'Authorization' => 'Basic '. base64_encode("{$this->user->email}:password")
            ])
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure([
                'data' => [
                    'name',
                    'periods',
                    'shift',
                ]
            ]);
    }

    /** @test */
    public function a_user_can_add_course()
    {
        $course = factory('App\Course')->make();

        $this->post(route('courses.store'), $course->toArray(), [
                'Authorization' => 'Basic '. base64_encode("{$this->user->email}:password")
            ])
            ->assertStatus(Response::HTTP_CREATED)
            ->assertJson($course->toArray());

        $this->assertDatabaseHas('courses', $course->toArray());
    }

    /** @test */
    public function a_user_can_edit_a_course()
    {
        $course = factory('App\Course')->create();

        $data = factory('App\Course')->make()->toArray();

        $this->put(route('courses.update', $course), $data, [
                'Authorization' => 'Basic '. base64_encode("{$this->user->email}:password")
            ])
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonFragment([true]);

        $this->assertDatabaseHas('courses', $data);
    }

    /** @test */
    public function a_user_can_delete_course()
    {
        $course = factory('App\Course')->create();

        $this->delete(route('courses.destroy', $course), [], [
                'Authorization' => 'Basic '. base64_encode("{$this->user->email}:password")
            ])
            ->assertStatus(Response::HTTP_NO_CONTENT)
            ->isEmpty();

        $this->assertDatabaseMissing('courses', $course->toArray());
    }
}
