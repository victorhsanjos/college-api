<?php

namespace Tests\Feature;

use Illuminate\Http\Response;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StudentScoreTest extends TestCase
{
    use RefreshDatabase;

    protected $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->artisan('db:seed --class RolesAndPermissionsSeeder');

        $this->user = factory('App\User')->states('teacher')->create()
            ->assignRole('teacher');
    }

    /** @test */
    public function teacher_can_add_a_student_score_on_the_subject()
    {
        $this->withoutExceptionHandling();

        $student = factory('App\Student')->create();
        $subject = factory('App\Subject')->create();

        $this->post("/api/subject/{$subject->id}/student/{$student->id}/score/add", ['score' => 10], [
                'Authorization' => 'Basic '. base64_encode("{$this->user->email}:password")
            ])
            ->assertStatus(Response::HTTP_CREATED)
            ->assertJson(['scored' => true]);

        $this->assertDatabaseHas('student_subject', ['student_id' => $student->id, 'subject_id' => $subject->id, 'score' => 10]);
    }

    /** @test */
    public function teacher_can_delete_a_student_score_on_the_subject()
    {
        $student = factory('App\Student')->create();
        $subject = factory('App\Subject')->create();

        $subject->students()->attach($student->id, ['score' => 10]);

        $this->delete("/api/subject/{$subject->id}/student/{$student->id}/score/delete", [], [
                'Authorization' => 'Basic '. base64_encode("{$this->user->email}:password")
            ])
            ->assertStatus(Response::HTTP_NO_CONTENT)
            ->isEmpty();

        $this->assertDatabaseMissing('student_subject', ['student_id' => $student->id, 'subject_id' => $subject->id, 'score' => 10]);
    }
}
