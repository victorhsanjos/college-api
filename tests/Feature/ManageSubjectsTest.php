<?php

namespace Tests\Feature;

use Illuminate\Http\Response;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ManageSubjectsTest extends TestCase
{
    use RefreshDatabase;

    protected $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->artisan('db:seed --class RolesAndPermissionsSeeder');

        $this->user = factory('App\User')->states('teacher')->create()
            ->assignRole('teacher');
    }

    /** @test */
    public function a_user_can_browser_subjects()
    {
        factory('App\Subject', 10)->create();

        $this->get(route('subjects.index'), [
                'Authorization' => 'Basic '. base64_encode("{$this->user->email}:password")
            ])
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure([
                'data' => [
                    [
                        'id',
                        'name',
                        'course_id',
                        'updated_at',
                        'created_at',
                    ]
                ],
                'links' => [
                    'first',
                    'last',
                    'prev',
                    'next',
                ],
                'meta' => [
                    'current_page',
                    'from',
                    'last_page',
                    'path',
                    'per_page',
                    'to',
                    'total'
                ]
            ]);
    }

    /** @test */
    public function a_user_can_search_subjects()
    {
        factory('App\Subject')->create(['name' => 'Development']);
        factory('App\Subject')->create(['name' => 'Crime']);
        factory('App\Subject')->create(['name' => 'Education']);
        factory('App\Subject')->create(['name' => 'Journalism']);
        factory('App\Subject')->create(['name' => 'English']);

        $this->get(route('subjects.index', 'q=Crime'), [
                'Authorization' => 'Basic '. base64_encode("{$this->user->email}:password")
            ])
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonCount(1, 'data');
    }

    /** @test */
    public function a_user_can_read_subject()
    {
        $subject = factory('App\Subject')->create();

        $this->get(route('subjects.show', $subject), [
                'Authorization' => 'Basic '. base64_encode("{$this->user->email}:password")
            ])
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure([
                'data' => [
                    'name',
                    'course_id',
                ]
            ]);
    }

    /** @test */
    public function a_user_can_add_subject()
    {
        $subject = factory('App\Subject')->make();

        $this->post(route('subjects.store'), $subject->toArray(), [
                'Authorization' => 'Basic '. base64_encode("{$this->user->email}:password")
            ])
            ->assertStatus(Response::HTTP_CREATED)
            ->assertJson($subject->toArray());

        $this->assertDatabaseHas('subjects', $subject->toArray());
    }

    /** @test */
    public function a_user_can_edit_a_subject()
    {
        $subject = factory('App\Subject')->create();

        $data = factory('App\Subject')->make()->toArray();

        $this->put(route('subjects.update', $subject), $data, [
                'Authorization' => 'Basic '. base64_encode("{$this->user->email}:password")
            ])
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonFragment([true]);

        $this->assertDatabaseHas('subjects', $data);
    }

    /** @test */
    public function a_user_can_delete_subject()
    {
        $subject = factory('App\Subject')->create();

        $this->delete(route('subjects.destroy', $subject), [], [
                'Authorization' => 'Basic '. base64_encode("{$this->user->email}:password")
            ])
            ->assertStatus(Response::HTTP_NO_CONTENT)
            ->isEmpty();

        $this->assertDatabaseMissing('subjects', $subject->toArray());
    }
}
