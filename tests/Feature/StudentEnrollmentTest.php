<?php

namespace Tests\Feature;

use Illuminate\Http\Response;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StudentEnrollmentTest extends TestCase
{
    use RefreshDatabase;

    protected $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->artisan('db:seed --class RolesAndPermissionsSeeder');

        $this->user = factory('App\User')->states('coodinator')->create()
            ->assignRole('coordinator');
    }

    /** @test */
    public function director_can_associate_a_student_to_course()
    {
        $this->withoutExceptionHandling();

        $student = factory('App\Student')->create();
        $course = factory('App\Course')->create();

        $this->post("/api/course/{$course->id}/student/{$student->id}/add", [], [
                'Authorization' => 'Basic '. base64_encode("{$this->user->email}:password")
            ])
            ->assertStatus(Response::HTTP_CREATED)
            ->assertJson(['enrolled' => true]);

        $this->assertDatabaseHas('course_student', ['course_id' => $course->id, 'student_id' => $student->id]);
    }

    /** @test */
    public function director_can_disassociate_a_student_to_course()
    {
        $student = factory('App\Student')->create();
        $course = factory('App\Course')->create();

        $course->students()->attach($student->id);

        $this->delete("/api/course/{$course->id}/student/{$student->id}/delete", [], [
                'Authorization' => 'Basic '. base64_encode("{$this->user->email}:password")
            ])
            ->assertStatus(Response::HTTP_NO_CONTENT)
            ->isEmpty();

        $this->assertDatabaseMissing('course_student', ['course_id' => $course->id, 'student_id' => $student->id]);
    }
}
