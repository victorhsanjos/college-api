<?php

namespace Tests\Feature;

use Illuminate\Http\Response;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ManageStudentsTest extends TestCase
{
    use RefreshDatabase;

    protected $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->artisan('db:seed --class RolesAndPermissionsSeeder');

        $this->user = factory('App\User')->states('teacher')->create()
            ->assignRole('teacher');
    }

    /** @test */
    public function a_user_can_browser_students()
    {
        factory('App\Student', 10)->create();

        $this->get(route('students.index'), [
                'Authorization' => 'Basic '. base64_encode("{$this->user->email}:password")
            ])
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure([
                'data' => [
                    [
                        'id',
                        'name',
                        'email',
                        'email_verified_at',
                        'registration',
                        'updated_at',
                        'created_at',
                    ]
                ],
                'links' => [
                    'first',
                    'last',
                    'prev',
                    'next',
                ],
                'meta' => [
                    'current_page',
                    'from',
                    'last_page',
                    'path',
                    'per_page',
                    'to',
                    'total'
                ]
            ]);
    }

    /** @test */
    public function a_user_can_search_students()
    {
        factory('App\User')->states('student')->create(['name' => 'Victor']);
        factory('App\User')->states('student')->create(['name' => 'Mizael']);
        factory('App\User')->states('student')->create(['name' => 'Luiz']);
        factory('App\User')->states('student')->create(['name' => 'Blade']);
        factory('App\User')->states('student')->create(['name' => 'Ken']);

        $this->get(route('students.index', 'q=Mizael'), [
                'Authorization' => 'Basic '. base64_encode("{$this->user->email}:password")
            ])
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonCount(1, 'data');
    }

    /** @test */
    public function a_user_can_read_student()
    {
        $student = factory('App\Student')->create();

        $this->get(route('students.show', $student), [
                'Authorization' => 'Basic '. base64_encode("{$this->user->email}:password")
            ])
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure([
                'data' => [
                    'name',
                    'email',
                    'email_verified_at',
                    'registration',
                ]
            ]);
    }

    /** @test */
    public function a_user_can_add_student()
    {
        $student = factory('App\Student')->make();
        $user = factory('App\User')->make();

        $data = array_merge(
            $student->toArray(),
            $user->toArray(),
            ['password' => '123456', 'password_confirmation' => '123456']
        );

        $this->post(route('students.store'), $data, [
                'Authorization' => 'Basic '. base64_encode("{$this->user->email}:password")
            ])
            ->assertStatus(Response::HTTP_CREATED)
            ->assertJson($student->toArray());

        $this->assertDatabaseHas('students', $student->toArray());
        $this->assertDatabaseHas('users', $user->toArray());
    }

    /** @test */
    public function a_user_can_edit_a_student()
    {
        factory('App\Student')->create();

        $student = factory('App\Student')->make();
        $user = factory('App\User')->make();

        $data = array_merge(
            $student->toArray(),
            $user->toArray()
        );

        $this->put(route('students.update', 1), $data, [
                'Authorization' => 'Basic '. base64_encode("{$this->user->email}:password")
            ])
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonFragment($student->toArray());

        $this->assertDatabaseHas('students', $student->toArray());
        $this->assertDatabaseHas('users', $user->toArray());
    }

    /** @test */
    public function a_user_can_delete_student()
    {
        $student = factory('App\Student')->create();

        $this->delete(route('students.destroy', $student), [], [
                'Authorization' => 'Basic '. base64_encode("{$this->user->email}:password")
            ])
            ->assertStatus(Response::HTTP_NO_CONTENT)
            ->isEmpty();

        $this->assertDatabaseMissing('students', $student->toArray());
        $this->assertDatabaseMissing('users', $student->toArray());
    }
}
