# College API

A API foi desenvolvida utilizando o Framework Laravel, 
juntamente com alguns pacotes que foram instalados durante 
o seu desenvolvimento. Algumas funcionalidades foram retiradas do
controlador e transformadas em outro controlador, mantendo assim o princípio de 
responsabilidade única e o [Cruddy by Design](https://www.youtube.com/watch?v=MF0jFKvS4SI).

# Instalação

```
$ git clone https://victorhsanjos@bitbucket.org/victorhsanjos/college-api.git
$ cd college-api
$ composer install
$ cp .env.example .env
$ php artisan key:generate
$ php artisan migrate
$ php artisan db:seed
$ php artisan serve
```

# Usuários para o acesso

| E-mail               | Senha    | Acesso      |
|----------------------|----------|-------------|
| ebelmiro@sjcc.com.br | password | Estudante   |
| mizael@sjcc.com.br   | password | Professor   |
| luiz@sjcc.com.br     | password | coordenador |

# Autenticação (Basic Authentication)

**Precisa ser informado no Header da requisição**

Authorization: basic base64("email:password")

*Exemplo*:

Authorization: basic base64_encode("ebelmiro@sjcc.com.br:password")

=> Authorization: basic ZWJlbG1pcm9Ac2pjYy5jb20uYnI6cGFzc3dvcmQ=

**Observação**: Recomendo a utilização do [Insomia](https://insomnia.rest/) para testa a API.


# URLs da API

| Method     | URI                                                  | Name             |
|------------|------------------------------------------------------|------------------|
| POST       | api/course/{course}/student/{student}/add            |                  |
| DELETE     | api/course/{course}/student/{student}/delete         |                  |
| POST       | api/courses                                          | courses.store    |
| GET\|HEAD  | api/courses                                          | courses.index    |
| GET\|HEAD  | api/courses/{course}                                 | courses.show     |
| PUT\|PATCH | api/courses/{course}                                 | courses.update   |
| DELETE     | api/courses/{course}                                 | courses.destroy  |
| GET\|HEAD  | api/students                                         | students.index   |
| POST       | api/students                                         | students.store   |
| GET|HEAD   | api/students/{student}                               | students.show    |
| PUT|PATCH  | api/students/{student}                               | students.update  |
| DELETE     | api/students/{student}                               | students.destroy |
| POST       | api/subject/{subject}/student/{student}/score/add    |                  |
| DELETE     | api/subject/{subject}/student/{student}/score/delete |                  |
| GET\|HEAD  | api/subjects                                         | subjects.index   |
| POST       | api/subjects                                         | subjects.store   |
| DELETE     | api/subjects/{subject}                               | subjects.destroy |
| PUT\|PATCH | api/subjects/{subject}                               | subjects.update  |
| GET\|HEAD  | api/subjects/{subject}                               | subjects.show    |
| GET\|HEAD  | api/user                                             |                  |

# Testes

A API foi testada com o uso do PHPUnit. Para rodar os testes execute o comando: 
```
$  vendor/bin/phpunit
```