<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Coordinator;
use Faker\Generator as Faker;

$factory->define(Coordinator::class, function (Faker $faker) {
    return [
        'salary' => $faker->randomFloat(),
    ];
});
