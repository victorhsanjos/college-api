<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Subject;
use Faker\Generator as Faker;

$factory->define(Subject::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->word,
        'course_id' => function () {
            return factory(App\Course::class)->create()->id;
        }
    ];
});
