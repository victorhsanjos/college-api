<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Course;
use Faker\Generator as Faker;

$factory->define(Course::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->word,
        'periods' => $faker->randomDigitNotNull,
        'shift' => $faker->randomDigitNotNull
    ];
});
