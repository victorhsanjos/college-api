<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Student;
use Faker\Generator as Faker;

$factory->define(Student::class, function (Faker $faker) {
    return [
        'registration' => (string) $faker->unique()->randomNumber(),
    ];
});

$factory->afterCreating(Student::class, function ($student, $faker) {
    factory(App\User::class)->create([
        'typeable_id' => $student->id,
        'typeable_type' => get_class($student)
    ]);
});
