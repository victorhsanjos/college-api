<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        Permission::create(['name' => 'browse students']);
        Permission::create(['name' => 'read students']);
        Permission::create(['name' => 'edit students']);
        Permission::create(['name' => 'add students']);
        Permission::create(['name' => 'delete students']);

        Permission::create(['name' => 'browse courses']);
        Permission::create(['name' => 'read courses']);
        Permission::create(['name' => 'edit courses']);
        Permission::create(['name' => 'add courses']);
        Permission::create(['name' => 'delete courses']);

        Permission::create(['name' => 'browse subjects']);
        Permission::create(['name' => 'read subjects']);
        Permission::create(['name' => 'edit subjects']);
        Permission::create(['name' => 'add subjects']);
        Permission::create(['name' => 'delete subjects']);

        Permission::create(['name' => 'add student score']);
        Permission::create(['name' => 'delete student score']);

        Permission::create(['name' => 'associate student to course']);
        Permission::create(['name' => 'dissociate student from course']);


        // create roles and assign created permissions
        Role::create(['name' => 'student'])
            ->givePermissionTo([
                'browse courses',
                'browse subjects'
        ]);

        Role::create(['name' => 'teacher'])
            ->givePermissionTo([
                'browse students',
                'read students',
                'edit students',
                'add students',
                'delete students',
                'browse courses',
                'read courses',
                'edit courses',
                'add courses',
                'delete courses',
                'browse subjects',
                'read subjects',
                'edit subjects',
                'add subjects',
                'delete subjects',
                'add student score',
                'delete student score'
            ]);

        Role::create(['name' => 'coordinator'])
            ->givePermissionTo([
                'browse students',
                'read students',
                'edit students',
                'add students',
                'delete students',
                'browse courses',
                'read courses',
                'edit courses',
                'add courses',
                'delete courses',
                'browse subjects',
                'read subjects',
                'edit subjects',
                'add subjects',
                'delete subjects',
                'associate student to course',
                'dissociate student from course'
            ]);
    }
}
