<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
        |--------------------------------------------------------------------------
        | Student
        |--------------------------------------------------------------------------
        */

        $tudent = \App\Student::create([
            'registration' => '201912052145'
        ]);

        $student = \App\User::create([
            'name' => 'Ebelmiro',
            'email' => 'ebelmiro@sjcc.com.br',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password,
            'typeable_id' => $tudent->id,
            'typeable_type' => get_class($tudent)
        ]);

        $student->assignRole('student');

        /*
        |--------------------------------------------------------------------------
        | Teacher
        |--------------------------------------------------------------------------
        */

        $teacher = \App\Teacher::create([
            'salary' => 3000.00
        ]);

        $teacher = \App\User::create([
            'name' => 'Mizael',
            'email' => 'mizael@sjcc.com.br',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password,
            'typeable_id' => $teacher->id,
            'typeable_type' => get_class($teacher)

        ]);

        $teacher->assignRole('teacher');

        /*
        |--------------------------------------------------------------------------
        | Coordinator
        |--------------------------------------------------------------------------
        */

        $coordinator = \App\Coordinator::create([
            'salary' => 3000.00
        ]);

        $coordinator = \App\User::create([
            'name' => 'Luiz',
            'email' => 'luiz@sjcc.com.br',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password,
            'typeable_id' => $coordinator->id,
            'typeable_type' => get_class($coordinator)
        ]);

        $coordinator->assignRole('coordinator');
    }
}
